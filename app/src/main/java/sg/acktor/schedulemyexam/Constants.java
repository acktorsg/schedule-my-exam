package sg.acktor.schedulemyexam;

import sg.acktor.schedulemyexam.utils.alarm.DurationType;

public final class Constants {
    public static final String DEFAULT_TIME_FORMAT = "h:mma";
    public static final String DATE_TIME_FORMAT = "EEEE, d MMM\nh:mma";

    public static final String CURRENT_SEM = "2016_SEM_1";
    public static final String CURRENT_ASSESSMENT = "MSA";

    public static final String DEFAULT_DURATION = DurationType.HOUR_1;

    public static final int REQUEST_CODE_NOTIFY = 668439;

    public static final String KEY_ENABLE_NOTIFICATIONS = "enable_notifications",
            KEY_NOTIFICATION_DURATION = "notification_duration",
            KEY_PERSIST_NOTIFICATION = "persist_notification",
            KEY_EXAM = "exam",
            KEY_IS_FIRST_TIME = "is_first_time",
            KEY_CURRENT_SEM = "current_semester",
            KEY_CURRENT_ASSESSMENT = "current_assessment",
            KEY_NOTIFICATION_ID = "notification_id",
            KEY_SWIPE_BACK = "swipe_back";

    /**
     * Private constructor to prevent other classes from creating an instance of this class
     */
    private Constants() { throw new RuntimeException("This class should not be instantiated at all"); }
}