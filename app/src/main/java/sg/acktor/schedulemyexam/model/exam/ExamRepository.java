package sg.acktor.schedulemyexam.model.exam;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import sg.acktor.schedulemyexam.model.exam.room.ExamDao;
import sg.acktor.schedulemyexam.model.exam.room.ScheduleMyExamDatabase;

public final class ExamRepository implements ExamModel {
    private static final String TAG = "ExamRepository";

    private volatile static ExamRepository INSTANCE;

    public synchronized static ExamRepository getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ExamRepository(context);
        }

        return INSTANCE;
    }

    private ExamDao examDao;
    private List<Exam> cachedExams = new ArrayList<>();
    private volatile boolean isCacheDirty = true;

    private ExamRepository(Context context) {
        if (INSTANCE != null) {
            throw new RuntimeException("This object must be created using getInstance() only");
        }

        this.examDao = Room.databaseBuilder(context, ScheduleMyExamDatabase.class, "schedule_my_exam")
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        ContentValues VALUES = new ContentValues(10);

                        db.beginTransaction();

                        for (Exam exam : ExamRetriever.getAllExams(context)) {
                            VALUES.put("moduleCode", exam.getModuleCode());
                            VALUES.put("moduleName", exam.getModuleName());
                            VALUES.put("location", "");
                            VALUES.put("seatNumber", -1);
                            VALUES.put("dateTimeStart", exam.getDateTimeStart());
                            VALUES.put("duration", exam.getDuration());
                            VALUES.put("examType", exam.getExamType());
                            VALUES.put("specialInstructions", exam.getSpecialInstructions());
                            VALUES.put("color", exam.getColor());
                            VALUES.put("isStarred", false);

                            long insertCheck = db.insert("exam", SQLiteDatabase.CONFLICT_IGNORE, VALUES);
                            if (insertCheck < 0) {
                                Log.e(TAG, "addAllExams: Failed to insert exam " + exam.getModuleCode());
                            }
                        }

                        db.setTransactionSuccessful();
                        db.endTransaction();
                    }
                })
                .build()
                .examDao();
    }

    @Override
    public Single<List<Exam>> getAllExams() {
        if (!isCacheDirty) {
            return Single.just(cachedExams);
        }

        return Single.fromCallable(() -> {
            cachedExams.clear();
            cachedExams.addAll(examDao.getAllExams());

            synchronized (ExamRepository.this) {
                isCacheDirty = false;
            }

            return cachedExams;
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Single<Exam> getExam(String moduleCode) {
        if (!isCacheDirty) {
            for (Exam exam : cachedExams) {
                if (exam.getModuleCode().equals(moduleCode)) {
                    return Single.just(exam);
                }
            }
        }

        return Single.fromCallable(() -> {
            Exam exam = examDao.getExam(moduleCode);
            if (exam == null) {
                throw new IOException("Failed to retrieve exam with module code {" + moduleCode + "}");
            }

            return exam;
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Completable updateExam(final Exam exam) {
        return Completable.fromAction(() -> examDao.updateExam(exam)).subscribeOn(Schedulers.io());
    }

    @Override
    public Completable clearAllExams() {
        return Completable.fromAction(() -> {
            examDao.clearExams();
            cachedExams.clear();
        }).subscribeOn(Schedulers.io());
    }
}