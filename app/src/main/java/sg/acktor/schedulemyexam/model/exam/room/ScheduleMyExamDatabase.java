package sg.acktor.schedulemyexam.model.exam.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import sg.acktor.schedulemyexam.model.exam.Exam;

@Database(entities = {Exam.class}, version = 1)
public abstract class ScheduleMyExamDatabase extends RoomDatabase {
    public abstract ExamDao examDao();
}