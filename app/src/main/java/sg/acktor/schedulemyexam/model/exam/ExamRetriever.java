package sg.acktor.schedulemyexam.model.exam;

import android.content.Context;

import androidx.annotation.NonNull;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.model.exam.Exam;
import sg.acktor.schedulemyexam.utils.ColorGenerator;

public final class ExamRetriever {
    @NonNull
    public static List<Exam> getAllExams(@NonNull Context context) {
        List<Exam> exams = new ArrayList<>();

        CSVReader csvReader = new CSVReader(new InputStreamReader(
                context.getResources().openRawResource(R.raw.exams)
        ));

        for (String[] csvLine : csvReader) {
            exams.add(parseToExam(csvLine));
        }

        try {
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return exams;
    }

    private static Exam parseToExam(String[] csvLine) {
        long dateTimeStart = -1;
        try {
            Date dateTimeObj = new SimpleDateFormat("dd-MMM-yy h:mm aa", Locale.getDefault())
                    .parse(csvLine[2] + " " + csvLine[3]);
            if (dateTimeObj != null) {
                dateTimeStart = dateTimeObj.getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new Exam(
                csvLine[0],
                csvLine[1],
                csvLine[5],
                csvLine[6],
                dateTimeStart,
                Integer.parseInt(csvLine[4]),
                ColorGenerator.getRandomColor()
        );
    }
}