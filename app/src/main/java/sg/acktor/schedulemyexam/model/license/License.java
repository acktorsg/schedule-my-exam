package sg.acktor.schedulemyexam.model.license;

public final class License {
    private String name, link, license;

    public License(String name, String link, String license) {
        this.name = name;
        this.link = link;
        this.license = license;
    }

    public String getName() { return name; }

    public String getLink() { return link; }

    public String getLicense() { return license; }
}