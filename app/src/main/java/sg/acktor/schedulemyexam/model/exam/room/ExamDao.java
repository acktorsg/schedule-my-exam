package sg.acktor.schedulemyexam.model.exam.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import sg.acktor.schedulemyexam.model.exam.Exam;

@Dao
public interface ExamDao {
    @Insert
    void addExams(List<Exam> exams);

    @Query("SELECT * FROM exam WHERE moduleCode = :moduleCode")
    Exam getExam(String moduleCode);

    @Query("SELECT * FROM exam ORDER BY isStarred DESC, dateTimeStart, moduleCode")
    List<Exam> getAllExams();

    @Update
    void updateExam(Exam exam);

    @Query("DELETE FROM exam")
    void clearExams();
}