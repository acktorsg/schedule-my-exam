package sg.acktor.schedulemyexam.model.exam;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sg.acktor.schedulemyexam.model.exam.Exam;

public interface ExamModel {
    Single<Exam> getExam(String moduleCode);

    Single<List<Exam>> getAllExams();

    Completable updateExam(Exam exam);

    Completable clearAllExams();
}