package sg.acktor.schedulemyexam.model.license;

import android.content.Context;

import androidx.annotation.NonNull;

import com.opencsv.CSVReader;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import sg.acktor.schedulemyexam.R;

public final class LicenseRetriever {
    private static List<License> licenses = new ArrayList<>();

    private LicenseRetriever() { throw new RuntimeException("This class should not be instantiated at all"); }

    public static List<License> getLicenses(@NonNull final Context context) {
        if (licenses.isEmpty()) {
            CSVReader reader = new CSVReader(new InputStreamReader(context.getResources().openRawResource(R.raw.licenses)));
            for (String[] line : reader) {
                licenses.add(new License(line[0], line[1], line[2]));
            }
        }

        return licenses;
    }
}