package sg.acktor.schedulemyexam.model.exam;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public final class Exam {
    @PrimaryKey
    @NonNull
    private String moduleCode = "";
    private String moduleName = "", location = "", examType = "", specialInstructions = "";
    private long dateTimeStart = -1;
    private int seatNumber = -1, duration = -1, color = -1;
    private boolean isStarred = false;

    public Exam() {}

    public Exam(@NonNull String moduleCode, @NonNull String moduleName, @NonNull String location,
                int seatNumber, @NonNull String examType, @NonNull String specialInstructions,
                long dateTimeStart, int durationInMinutes, @ColorInt int color, boolean isStarred) {
        this.moduleCode = moduleCode;
        this.moduleName = moduleName;
        this.location = location;
        this.seatNumber = seatNumber;
        this.examType = examType;
        this.specialInstructions = specialInstructions;
        this.dateTimeStart = dateTimeStart;
        this.duration = durationInMinutes;
        this.color = color;
        this.isStarred = isStarred;
    }

    public Exam(@NonNull String moduleCode, @NonNull String moduleName, @NonNull String examType,
                @NonNull String specialInstructions, long dateTimeStart, int durationInMinutes,
                @ColorInt int color) {
        this(moduleCode, moduleName, "NOT SET", -1, examType, specialInstructions, dateTimeStart,
                durationInMinutes, color, false);
    }

    public void setModuleCode(@NonNull String moduleCode) { this.moduleCode = moduleCode; }

    public void setModuleName(String moduleName) { this.moduleName = moduleName; }

    public void setExamType(String examType) { this.examType = examType; }

    public void setSpecialInstructions(String specialInstructions) { this.specialInstructions = specialInstructions; }

    public void setDateTimeStart(long dateTimeStart) { this.dateTimeStart = dateTimeStart; }

    public void setDuration(int duration) { this.duration = duration; }

    @NonNull
    public String getModuleCode() { return moduleCode; }

    @NonNull
    public String getModuleName() { return moduleName; }

    @NonNull
    public String getLocation() { return location; }

    public void setLocation(@NonNull String location) { this.location = location; }

    public int getSeatNumber() { return seatNumber; }

    public void setSeatNumber(int seatNumber) { this.seatNumber = seatNumber; }

    @NonNull
    public String getExamType() { return examType; }

    @NonNull
    public String getSpecialInstructions() { return specialInstructions; }

    public long getDateTimeStart() { return dateTimeStart; }

    @ColorInt
    public int getColor() { return color; }

    public void setColor(@ColorInt int color) { this.color = color; }

    public int getDuration() { return duration; }

    public boolean isStarred() { return isStarred; }

    public void setStarred(boolean starred) { isStarred = starred; }
}