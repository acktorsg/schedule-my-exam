package sg.acktor.schedulemyexam.features.receivers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.ui.MainActivity;

public class NotifyReceiver extends BroadcastReceiver {
    private static final int OPEN_ACTIVITY_REQUEST_CODE = 6736, CANCEL_NOTIFICATION_REQUEST_CODE = 226235;

    @Override
    public void onReceive(Context context, Intent intent) {
//        Exam exam = intent.getParcelableExtra(Constants.KEY_EXAM);
//        if (exam == null) {
//            return;
//        }
//
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//        boolean isPersistent = prefs.getBoolean(Constants.KEY_PERSIST_NOTIFICATION, true);
//
//        String title = "Your " + exam.getModuleCode() + " exam is starting soon";
//        String location = !exam.getLocation().equals("NOT SET") ? exam.getLocation() : "the exam location";
//
//        Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle()
//                .setBigContentTitle(title)
//                .bigText(
//                        "Exam starts at " + exam.getDateAsString() + " " + exam.getStartTimeAsString()
//                                + "\n\nLocation: " + exam.getLocation()
//                                + "\nSeat Number: " + exam.getSeatNumber()
//                );
//
//        Notification notification = buildNotification(context, title,
//                "Report to " + location + " 15 mins before it starts", bigTextStyle, isPersistent);
//
//        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        if (nManager != null) {
//            nManager.notify(exam.generateId(), notification);
//        }
//
//        if (isPersistent) {
//            PendingIntent pIntent = PendingIntent.getBroadcast(
//                    context,
//                    CANCEL_NOTIFICATION_REQUEST_CODE,
//                    new Intent(context, UndoNotifyReceiver.class)
//                            .putExtra(Constants.KEY_NOTIFICATION_ID, exam.generateId()),
//                    PendingIntent.FLAG_CANCEL_CURRENT
//            );
//
//            AlarmManager aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//            if (aManager != null) {
//                aManager.set(AlarmManager.RTC_WAKEUP, exam.getDateTimeStart().getTimeInMillis(), pIntent);
//            }
//        }
    }

    /**
     * Build a BigTextStyle notification
     *
     * @param context      used for Builder constructor
     * @param title        notification title
     * @param content      notification content when not expanded
     * @param bigTextStyle style shown when expanded
     * @param isPersistent whether notification is ongoing
     * @return a Notification object built based on parameters
     */
    private Notification buildNotification(Context context, String title, String content,
                                           Notification.BigTextStyle bigTextStyle, boolean isPersistent) {
        return new Notification.Builder(context)
                .setOngoing(isPersistent)
                .setAutoCancel(!isPersistent)
                .setSmallIcon(R.drawable.ic_notify_icon)
                .setContentTitle(title)
                .setContentText(content)
                .setStyle(bigTextStyle)
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                OPEN_ACTIVITY_REQUEST_CODE,
                                new Intent(context, MainActivity.class),
                                PendingIntent.FLAG_CANCEL_CURRENT
                        )
                ).build();
    }
}