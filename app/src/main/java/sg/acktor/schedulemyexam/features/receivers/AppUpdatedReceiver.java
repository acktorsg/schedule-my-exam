package sg.acktor.schedulemyexam.features.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import sg.acktor.schedulemyexam.Constants;
import sg.acktor.schedulemyexam.model.exam.ExamRepository;

/**
 * @author Khairuddin Bin Ali
 * @version 1.0.0
 */
public final class AppUpdatedReceiver extends BroadcastReceiver {
    private Disposable disposable;

    @Override
    public void onReceive(Context context, Intent intent) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.getString(Constants.KEY_CURRENT_SEM, "").equals(Constants.CURRENT_SEM)) {
            return;
        }

        ExamRepository.getInstance(context).clearAllExams()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) { disposable = d; }

                    @Override
                    public void onComplete() {
                        prefs.edit().clear().apply();
                        disposable.dispose();
                    }

                    @Override
                    public void onError(Throwable e) { e.printStackTrace(); }
                });
    }
}