package sg.acktor.schedulemyexam.features.validation;

import android.text.TextUtils;

public final class SeatNumberValidator {
    private SeatNumberValidator() {
        throw new RuntimeException("This class should not be instantiated at all");
    }

    public static boolean checkSeatNumber(String seatNumber) {
        return !seatNumber.isEmpty() && TextUtils.isDigitsOnly(seatNumber);
    }
}