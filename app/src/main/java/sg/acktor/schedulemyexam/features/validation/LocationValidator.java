package sg.acktor.schedulemyexam.features.validation;

import java.util.regex.Pattern;

public final class LocationValidator {
    private LocationValidator() {
        throw new RuntimeException("This class should not be instantiated at all");
    }

    public static boolean checkLocation(String location) { return Pattern.matches("^[a-zA-Z]\\d{2}[a-zA-Z]$", location); }
}