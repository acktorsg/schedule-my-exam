package sg.acktor.schedulemyexam.features.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import sg.acktor.schedulemyexam.Constants;

public class UndoNotifyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = intent.getIntExtra(Constants.KEY_NOTIFICATION_ID, -1);
        if (notificationId != -1) {
            NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (nManager != null) {
                nManager.cancel(notificationId);
            }
        }
    }
}