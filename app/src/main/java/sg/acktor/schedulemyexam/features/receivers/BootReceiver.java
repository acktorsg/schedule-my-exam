package sg.acktor.schedulemyexam.features.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.reactivex.disposables.CompositeDisposable;
import sg.acktor.schedulemyexam.Constants;

public class BootReceiver extends BroadcastReceiver {
    private SharedPreferences prefs;
    private CompositeDisposable disposables;

    public BootReceiver() { this.disposables = new CompositeDisposable(); }

    @Override
    public void onReceive(final Context context, Intent intent) {
        String intentAction = intent.getAction();
        if (intentAction == null || (!intentAction.equals(Intent.ACTION_BOOT_COMPLETED)
                && !intentAction.equals(Intent.ACTION_LOCKED_BOOT_COMPLETED))) {
            return;
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!prefs.getBoolean(Constants.KEY_ENABLE_NOTIFICATIONS, false)) {
            return;
        }

//        ExamRepository.getInstance(context).getAllExams()
//                .subscribeOn(Schedulers.io())
//                .observeOn(Schedulers.computation())
//                .subscribe(new SingleObserver<List<Exam>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) { disposables.add(d); }
//
//                    @Override
//                    public void onSuccess(List<Exam> exams) {
//                        AndroidAlarmScheduler.getInstance(context)
//                                .setAlarmForExams(
//                                        prefs.getString(
//                                                Constants.KEY_NOTIFICATION_DURATION, Constants.DEFAULT_DURATION
//                                        ),
//                                        exams.toArray(new Exam[0])
//                                )
//                                .subscribeOn(Schedulers.computation())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribe(new CompletableObserver() {
//                                    @Override
//                                    public void onSubscribe(Disposable d) { disposables.add(d); }
//
//                                    @Override
//                                    public void onComplete() { disposables.clear(); }
//
//                                    @Override
//                                    public void onError(Throwable e) {
//                                        e.printStackTrace();
//                                        disposables.clear();
//                                    }
//                                });
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                        disposables.clear();
//                    }
//                });
    }
}