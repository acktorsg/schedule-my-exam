package sg.acktor.schedulemyexam.features;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import sg.acktor.schedulemyexam.model.exam.Exam;
import sg.acktor.schedulemyexam.model.exam.ExamModel;
import sg.acktor.schedulemyexam.model.exam.ExamRepository;

public final class ExamListViewModel extends AndroidViewModel {
    private ExamModel examRepo;

    private CompositeDisposable disposables = new CompositeDisposable();

    public ExamListViewModel(@NonNull Application application) {
        super(application);
        examRepo = ExamRepository.getInstance(application);
    }

    public void getAllExams(Consumer<List<Exam>> onSuccess, Consumer<Throwable> onError) {
        disposables.add(
                examRepo.getAllExams().observeOn(AndroidSchedulers.mainThread())
                        .subscribe(onSuccess, onError)
        );
    }

    public void updateExam(Exam exam, Consumer<Throwable> onError) {
        disposables.add(
                examRepo.updateExam(exam).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {}, onError)
        );
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }
}