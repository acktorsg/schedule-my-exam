package sg.acktor.schedulemyexam.utils;

import android.graphics.Color;

import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;

import java.util.Random;

public final class ColorGenerator {
    private static final Random RANDOM = new Random(System.currentTimeMillis());

    private ColorGenerator() { throw new RuntimeException("This class should not be instantiated at all"); }

    @ColorInt
    public static int getRandomColor() {
        int color = Color.rgb(RANDOM.nextInt(255), RANDOM.nextInt(255), RANDOM.nextInt(255));
        return ColorUtils.calculateLuminance(color) > 0.5 ? getRandomColor() : color;
    }
}