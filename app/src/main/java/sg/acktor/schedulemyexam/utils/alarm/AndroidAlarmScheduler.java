package sg.acktor.schedulemyexam.utils.alarm;

import android.app.AlarmManager;
import android.content.Context;

import java.lang.ref.WeakReference;

import io.reactivex.Completable;
import io.reactivex.functions.Action;
import sg.acktor.schedulemyexam.model.exam.Exam;

/**
 * @author Khairuddin Bin Ali
 * @version 1.0.0
 */
public final class AndroidAlarmScheduler implements AlarmScheduler {
    private static final String TAG = "AndroidAlarmScheduler";

    private static AndroidAlarmScheduler INSTANCE;

    private WeakReference<AlarmManager> weakAlarmManager;
    private WeakReference<Context> weakContext;

    private AndroidAlarmScheduler(Context context) {
        this.weakContext = new WeakReference<>(context);
        this.weakAlarmManager = new WeakReference<>(
                (AlarmManager) context.getSystemService(Context.ALARM_SERVICE)
        );
    }

    public static AndroidAlarmScheduler getInstance(Context context) {
        if (INSTANCE == null || !INSTANCE.isContextAvailable()) {
            INSTANCE = new AndroidAlarmScheduler(context);
        }

        return INSTANCE;
    }

    @Override
    public Completable setAlarmForExams(final String durationType, final Exam... exams) {
        return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Context context = weakContext.get();
                if (context == null) {
                    throw new Exception("Could not retrieve instance of Context");
                }

                AlarmManager alarmManager = weakAlarmManager.get();
                if (alarmManager == null) {
                    throw new Exception("Could not retrieve instance of AlarmManager");
                }

//                for (Exam exam : exams) {
//                    if (exam.hasPassedStartTime()) {
//                        continue;
//                    }
//
//                    long triggerInMillis = AlarmUtils.getNotificationDelay(
//                            durationType, exam.getDateTimeStart()
//                    );
//                    PendingIntent pIntent = createNotificationPendingIntent(context, exam);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, delay, pIntent);
//                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, triggerInMillis, pIntent);
//                    } else {
//                        alarmManager.set(AlarmManager.RTC_WAKEUP, triggerInMillis, pIntent);
//                    }
//                }
            }
        });
    }

    private boolean isContextAvailable() { return weakContext.get() != null; }
}