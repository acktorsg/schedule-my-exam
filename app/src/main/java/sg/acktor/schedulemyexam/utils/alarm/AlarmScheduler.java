package sg.acktor.schedulemyexam.utils.alarm;

import io.reactivex.Completable;
import sg.acktor.schedulemyexam.model.exam.Exam;

/**
 * @author Khairuddin Bin Ali
 * @version 1.0.0
 */
public interface AlarmScheduler {
    Completable setAlarmForExams(String durationType, Exam... exams);
}