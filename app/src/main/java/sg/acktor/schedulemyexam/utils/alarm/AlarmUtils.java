package sg.acktor.schedulemyexam.utils.alarm;

import java.util.Calendar;

/**
 * @author Khairuddin Bin Ali
 * @version 1.0.0
 */
public final class AlarmUtils {

    private AlarmUtils() {}

    public static long getNotificationDelay(String durationType, Calendar whenToNotify) {
        Calendar dateTimeCopy = (Calendar) whenToNotify.clone();

        int duration = 0;

        switch (durationType) {
            case DurationType.MINUTE_15:
                duration -= 15;
                break;
            case DurationType.MINUTE_30:
                duration -= 30;
                break;
        }
        switch (durationType) {
            case DurationType.MINUTE_15:
                duration -= 15;
                break;
            case DurationType.MINUTE_30:
                duration -= 30;
                break;
        }
        dateTimeCopy.add(Calendar.MINUTE, duration);
        duration = 0;

        switch (durationType) {
            case DurationType.HOUR_1:
                duration -= 1;
                break;
            case DurationType.HOUR_2:
                duration -= 2;
                break;
            case DurationType.HOUR_4:
                duration -= 4;
                break;
            case DurationType.HOUR_8:
                duration -= 8;
                break;
        }
        dateTimeCopy.add(Calendar.HOUR, duration);
        duration = 0;

        switch (durationType) {
            case DurationType.DAY_1:
                duration -= 1;
                break;
            case DurationType.DAY_2:
                duration -= 2;
                break;
        }
        dateTimeCopy.add(Calendar.DAY_OF_YEAR, duration);

        return dateTimeCopy.getTimeInMillis();
    }
}