package sg.acktor.schedulemyexam.utils.alarm;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.StringDef;

/**
 * @author Khairuddin Bin Ali
 */
@Retention(RetentionPolicy.SOURCE)
@StringDef({
        DurationType.MINUTE_15, DurationType.MINUTE_30,
        DurationType.HOUR_1, DurationType.HOUR_2, DurationType.HOUR_4, DurationType.HOUR_8,
        DurationType.DAY_1, DurationType.DAY_2
})
public @interface DurationType {
    String MINUTE_15 = "15 minutes", MINUTE_30 = "30 minutes";

    String HOUR_1 = "1 hour", HOUR_2 = "2 hours", HOUR_4 = "4 hours", HOUR_8 = "8 hours";

    String DAY_1 = "1 day", DAY_2 = "2 days";
}