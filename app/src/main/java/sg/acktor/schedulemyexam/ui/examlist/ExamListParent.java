package sg.acktor.schedulemyexam.ui.examlist;

import sg.acktor.schedulemyexam.model.exam.Exam;

public interface ExamListParent {
    void updateExam(Exam exam);
}