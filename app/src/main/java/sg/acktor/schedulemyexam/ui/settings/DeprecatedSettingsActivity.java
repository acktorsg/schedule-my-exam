package sg.acktor.schedulemyexam.ui.settings;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import sg.acktor.schedulemyexam.Constants;
import sg.acktor.schedulemyexam.R;

public class DeprecatedSettingsActivity extends AppCompatActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {
    SwitchCompat swNotificationActivate, swPersistentNotification;
    RelativeLayout rlNotificationActivate, rlNotificationDuration, rlPersistentNotification;
    TextView tvNotifyDuration;

    SharedPreferences prefs;
    String[] durations;
    String selectedDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_deprecated);
        setViews();

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        durations = getResources().getStringArray(R.array.notify_durations);

        swPersistentNotification.setChecked(prefs.getBoolean(Constants.KEY_PERSIST_NOTIFICATION, true));

        selectedDuration = prefs.getString(Constants.KEY_NOTIFICATION_DURATION, Constants.DEFAULT_DURATION);
        tvNotifyDuration.setText(selectedDuration);

        boolean isNotificationEnabled = prefs.getBoolean(Constants.KEY_ENABLE_NOTIFICATIONS, false);
        swNotificationActivate.setChecked(isNotificationEnabled);
        setNotificationSettingsEnabled(isNotificationEnabled);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setElevation(0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == rlNotificationActivate) {
            swNotificationActivate.setChecked(!swNotificationActivate.isChecked());
        } else if (v == rlNotificationDuration) {
            new AlertDialog.Builder(this)
                    .setAdapter(new DurationAdapter(durations), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedDuration = durations[which];

                            prefs.edit().putString(Constants.KEY_NOTIFICATION_DURATION, selectedDuration).apply();
                            if (swNotificationActivate.isChecked()) {
//                                Helper.cancelAlarms(getApplicationContext());
//                                Helper.setAlarmss(getApplicationContext());
                            }

                            tvNotifyDuration.setText(selectedDuration);
                        }
                    })
                    .show();
        } else if (v == rlPersistentNotification) {
            swPersistentNotification.setChecked(!swPersistentNotification.isChecked());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switchNofiticationActivate:
//                if (isChecked) {
//                    Helper.setAlarms(getApplicationContext());
//                } else {
//                    Helper.cancelAlarms(getApplicationContext());
//                }

                setNotificationSettingsEnabled(isChecked);
                prefs.edit().putBoolean(Constants.KEY_ENABLE_NOTIFICATIONS, isChecked).apply();

                break;
            case R.id.switchPersistentNotification:
                prefs.edit().putBoolean(Constants.KEY_PERSIST_NOTIFICATION, isChecked).apply();
                break;
        }
    }

    /**
     * Set enabled state for rows of notification settings
     *
     * @param enabled true if notifications are enabled, false otherwise
     */
    private void setNotificationSettingsEnabled(boolean enabled) {
        rlNotificationDuration.setEnabled(enabled);
        rlPersistentNotification.setEnabled(enabled);
        swPersistentNotification.setEnabled(enabled);
    }

    /**
     * Finds views
     */
    private void setViews() {
        swNotificationActivate = findViewById(R.id.switchNofiticationActivate);
        swPersistentNotification = findViewById(R.id.switchPersistentNotification);
        rlNotificationActivate = findViewById(R.id.relativeLayoutNotificationActivate);
        rlNotificationDuration = findViewById(R.id.relativeLayoutNotificationDuration);
        rlPersistentNotification = findViewById(R.id.relativeLayoutPersistentNotification);
        tvNotifyDuration = findViewById(R.id.textViewNotifyDurationSet);

        rlNotificationActivate.setOnClickListener(this);
        rlNotificationDuration.setOnClickListener(this);
        rlPersistentNotification.setOnClickListener(this);

        swNotificationActivate.setOnCheckedChangeListener(this);
        swPersistentNotification.setOnCheckedChangeListener(this);
    }
}