package sg.acktor.schedulemyexam.ui.settings;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

public final class DurationAdapter implements ListAdapter {
    private String[] durations;

    public DurationAdapter(String[] durations) { this.durations = durations; }

    @Override
    public boolean areAllItemsEnabled() { return true; }

    @Override
    public boolean isEnabled(int position) { return true; }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {}

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {}

    @Override
    public int getCount() { return durations.length; }

    @Override
    public Object getItem(int position) { return durations[position]; }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public boolean hasStableIds() { return true; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        ((TextView) convertView.findViewById(android.R.id.text1)).setText(durations[position]);

        return convertView;
    }

    @Override
    public int getItemViewType(int position) { return 0; }

    @Override
    public int getViewTypeCount() { return 1; }

    @Override
    public boolean isEmpty() { return durations.length == 0; }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() { return new CharSequence[0]; }
}
