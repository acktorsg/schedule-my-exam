package sg.acktor.schedulemyexam.ui.examlist;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.features.ExamListViewModel;
import sg.acktor.schedulemyexam.model.exam.Exam;
import sg.acktor.schedulemyexam.ui.MainActivity;
import sg.acktor.schedulemyexam.ui.examlist.adapter.ExamAdapter;

public final class ExamListFragment extends Fragment implements ExamListParent {
    private CoordinatorLayout rootLayout;

    private ExamListViewModel examListViewModel;
    private ExamAdapter examAdapter;

    public ExamListFragment() { super(R.layout.fragment_exam_list); }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        examListViewModel = ViewModelProviders.of(this).get(ExamListViewModel.class);
        examAdapter = new ExamAdapter(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        rootLayout = view.findViewById(R.id.rootLayout);
        Toolbar tbExamList = view.findViewById(R.id.toolbarExamList);
        RecyclerView rvExams = view.findViewById(R.id.recyclerViewExams);

        tbExamList.inflateMenu(R.menu.menu_main);
        tbExamList.setOnClickListener(v -> rvExams.smoothScrollToPosition(0));
        tbExamList.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_settings:
                    // Show settings
                    return false;
                case R.id.menu_info:
                    // Show about page
                    ((MainActivity) requireActivity()).showAboutUi();
                    return true;
                default:
                    return false;
            }
        });

        rvExams.setAdapter(examAdapter);

        examListViewModel.getAllExams(exams -> {
            examAdapter.submitList(exams);
        }, e -> {
            e.printStackTrace();
            showErrorMessage(getString(R.string.message_error_exams_load));
        });
    }

    @Override
    public void updateExam(Exam exam) {
        examListViewModel.updateExam(exam, (e) -> {
            e.printStackTrace();
            showErrorMessage("Failed to update " + exam.getModuleCode() + " exam");
        });
    }

    private void showErrorMessage(String errorMessage) {
        Snackbar.make(rootLayout, errorMessage, Snackbar.LENGTH_SHORT).show();
    }
}