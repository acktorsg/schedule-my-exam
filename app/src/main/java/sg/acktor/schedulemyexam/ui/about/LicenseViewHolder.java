package sg.acktor.schedulemyexam.ui.about;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.model.license.License;

final class LicenseViewHolder extends RecyclerView.ViewHolder {
    private TextView tvLibNameLink, tvLicense;

    LicenseViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_license, parent, false));

        tvLibNameLink = itemView.findViewById(R.id.textViewLibNameLink);
        tvLicense = itemView.findViewById(R.id.textViewLicense);
    }

    void bindHolder(License license) {
        tvLibNameLink.setText(itemView.getContext().getString(
                R.string.format_licenseName, license.getName(), license.getLink()
        ));
        tvLicense.setText(license.getLicense());
    }
}