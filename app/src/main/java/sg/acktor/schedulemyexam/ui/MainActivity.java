package sg.acktor.schedulemyexam.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.ui.about.AboutFragment;
import sg.acktor.schedulemyexam.ui.examlist.ExamListFragment;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportFragmentManager().findFragmentById(R.id.fragmentHolder) == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentHolder, new ExamListFragment())
                    .commit();
        }
    }

    public void showAboutUi() {
        // TODO: 2/8/2019 Add animation
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentHolder, new AboutFragment())
                .addToBackStack(null)
                .commit();
    }
}