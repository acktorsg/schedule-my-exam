package sg.acktor.schedulemyexam.ui.examlist.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import sg.acktor.schedulemyexam.model.exam.Exam;
import sg.acktor.schedulemyexam.ui.examlist.ExamListParent;

public class ExamAdapter extends ListAdapter<Exam, ExamViewHolder> {
    private ExamListParent parentView;

    public ExamAdapter(ExamListParent parentView) {
        super(new DiffUtil.ItemCallback<Exam>() {
            @Override
            public boolean areItemsTheSame(@NonNull Exam oldItem, @NonNull Exam newItem) {
                return oldItem.getModuleCode().equals(newItem.getModuleCode());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Exam oldItem, @NonNull Exam newItem) {
                return oldItem.getLocation().equals(newItem.getLocation())
                        && oldItem.getSeatNumber() == newItem.getSeatNumber()
                        && oldItem.isStarred() == newItem.isStarred();
            }
        });

        this.parentView = parentView;
    }

    @NonNull
    @Override
    public ExamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExamViewHolder(parent, parentView);
    }

    @Override
    public void onBindViewHolder(@NonNull ExamViewHolder holder, int position) {
        holder.bindHolder(getItem(position));
    }
}