package sg.acktor.schedulemyexam.ui.examlist.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.TimeUnit;

import sg.acktor.schedulemyexam.Constants;
import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.features.validation.LocationValidator;
import sg.acktor.schedulemyexam.features.validation.SeatNumberValidator;
import sg.acktor.schedulemyexam.model.exam.Exam;
import sg.acktor.schedulemyexam.ui.examlist.ExamListParent;

final class ExamViewHolder extends RecyclerView.ViewHolder {
    private TextView tvModuleCode, tvModuleName, tvDateTime, tvInstructions;
    private TextInputLayout tiLocation, tiSeatNumber;
    private CheckBox cbStarred;

    private Exam exam;

    ExamViewHolder(ViewGroup parent, ExamListParent parentView) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_exam, parent, false));

        tvModuleCode = itemView.findViewById(R.id.textViewModuleCode);
        tvModuleName = itemView.findViewById(R.id.textViewModuleName);
        tvDateTime = itemView.findViewById(R.id.textViewDateTime);
        tiLocation = itemView.findViewById(R.id.textInputLocation);
        tiSeatNumber = itemView.findViewById(R.id.textInputSeatNumber);
        tvInstructions = itemView.findViewById(R.id.textViewInstructions);
        cbStarred = itemView.findViewById(R.id.checkBoxStarred);

        View.OnFocusChangeListener focusChangeListener = (view, hasFocus) -> {
            EditText userInputText = (EditText) view;
            if (hasFocus) {
                if (userInputText.getText().toString().equals("NOT SET")) {
                    userInputText.getText().clear();
                }
            } else {
                if (userInputText.getText().toString().isEmpty()) {
                    userInputText.setText("NOT SET");
                }
            }
        };

        EditText etLocation = getLocationEditText();
        etLocation.setOnFocusChangeListener(focusChangeListener);
        etLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence text, int i, int i1, int i2) {
                String newLocation = text.toString();
                if (newLocation.equals(exam.getLocation())) {
                    return;
                }

                if (!LocationValidator.checkLocation(newLocation)) {
                    // Show error
                    tiLocation.setBoxBackgroundColorResource(R.color.red_500);
                    return;
                }

                tiLocation.setBoxBackgroundColor(exam.getColor());
                exam.setLocation(newLocation);
                parentView.updateExam(exam);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        EditText etSeatNumber = getSeatNumberEditText();
        etSeatNumber.setOnFocusChangeListener(focusChangeListener);
        etSeatNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence text, int i, int i1, int i2) {
                String newSeatNumber = text.toString();
                if (!SeatNumberValidator.checkSeatNumber(newSeatNumber)) {
                    return;
                }

                int seatNumber = Integer.parseInt(newSeatNumber);
                if (seatNumber == exam.getSeatNumber()) {
                    return;
                }

                exam.setSeatNumber(Integer.parseInt(newSeatNumber));
                parentView.updateExam(exam);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        tiLocation.setVisibility(View.GONE);
        tiSeatNumber.setVisibility(View.GONE);

        cbStarred.setOnCheckedChangeListener((view, isChecked) -> {
            int visibility = isChecked ? View.VISIBLE : View.GONE;
            tiLocation.setVisibility(visibility);
            tiSeatNumber.setVisibility(visibility);

            if (isChecked == exam.isStarred()) {
                return;
            }

            exam.setStarred(isChecked);
            parentView.updateExam(exam);
        });
    }

    void bindHolder(@NonNull Exam exam) {
        this.exam = exam;

        tvModuleCode.setTextColor(exam.getColor());
        tvModuleCode.setText(exam.getModuleCode());
        tvDateTime.setText(itemView.getResources().getString(
                R.string.format_timing,
                DateFormat.format(Constants.DATE_TIME_FORMAT, exam.getDateTimeStart()).toString(),
                DateFormat.format(
                        Constants.DEFAULT_TIME_FORMAT,
                        exam.getDateTimeStart() + TimeUnit.MINUTES.toMillis(exam.getDuration())
                ).toString(),
                exam.getDuration()
        ));
        tvModuleName.setText(exam.getModuleName());
        getLocationEditText().setText(exam.getLocation());
        getSeatNumberEditText().setText(seatNumberAsText(exam.getSeatNumber()));
        tvInstructions.setText(exam.getSpecialInstructions());
        cbStarred.setChecked(exam.isStarred());

        tiLocation.setBoxBackgroundColor(exam.getColor());
        tiSeatNumber.setBoxBackgroundColor(exam.getColor());
    }

    private String seatNumberAsText(int seatNumber) {
        if (seatNumber < 0) { return "NOT SET"; } else { return "" + seatNumber; }
    }

    private EditText getLocationEditText() {
        EditText etLocation = tiLocation.getEditText();
        return etLocation == null ? itemView.findViewById(R.id.editTextLocation) : etLocation;
    }

    private EditText getSeatNumberEditText() {
        EditText etLocation = tiSeatNumber.getEditText();
        return etLocation == null ? itemView.findViewById(R.id.editTextSeatNumber) : etLocation;
    }
}