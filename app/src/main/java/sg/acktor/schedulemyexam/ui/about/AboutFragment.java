package sg.acktor.schedulemyexam.ui.about;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import sg.acktor.schedulemyexam.BuildConfig;
import sg.acktor.schedulemyexam.R;
import sg.acktor.schedulemyexam.model.license.LicenseRetriever;

public final class AboutFragment extends Fragment {
    public AboutFragment() { super(R.layout.fragment_about); }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        // Display app version
        ((TextView) view.findViewById(R.id.textViewAppVersion)).setText(BuildConfig.VERSION_NAME);

        Toolbar tbAbout = view.findViewById(R.id.toolbarAbout);
        tbAbout.setNavigationOnClickListener(v -> requireActivity().onBackPressed());

        ((RecyclerView) view.findViewById(R.id.recyclerViewLicenses)).setAdapter(
                new LicenseAdapter(LicenseRetriever.getLicenses(requireContext()))
        );
    }
}