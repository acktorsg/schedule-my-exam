package sg.acktor.schedulemyexam.ui.about;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import sg.acktor.schedulemyexam.model.license.License;

final class LicenseAdapter extends RecyclerView.Adapter<LicenseViewHolder> {
    private List<License> licenses;

    LicenseAdapter(List<License> licenses) { this.licenses = licenses; }

    @NonNull
    @Override
    public LicenseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LicenseViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull LicenseViewHolder holder, int position) {
        holder.bindHolder(licenses.get(position));
    }

    @Override
    public int getItemCount() { return licenses.size(); }
}